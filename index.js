const puppeteer = require('puppeteer');
const argv = require('minimist')(process.argv.slice(2));
const chalk = require('chalk');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(argv.url);

  const hrefs = await page.evaluate(() => {
    const links = Array.from(document.links);
    return links.map(link =>  {
      return link.href;
    });
  });

  for await (const href of hrefs) {
    if (argv.all) {
      if (href.includes('https')) {
        console.log(chalk.green(href));
      } else {
        console.log(chalk.red(href));
      } 
    } else {
      if (!href.includes('https')) {
        console.log(chalk.red(href));
      }
    }
  } 

  await browser.close();

})();
