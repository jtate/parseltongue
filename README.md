# Parseltongue

Code that scrapes a page for links and outputs non-https links in red. 

## To run

To run, pass the url as an argument to the node command line. 

`node index.js --url=https://uw.edu/research`

If you add `--all` all page links are output. 
